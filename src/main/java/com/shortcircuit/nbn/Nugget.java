package com.shortcircuit.nbn;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

/**
 * @author ShortCircuit908
 *         Created on 1/8/2016
 */
public abstract class Nugget<T> {
	private static final String DEFAULT_TOSTRING_FORMAT = "%1$s[name=\"%2$s\", value=%3$s]";
	private static final String CUSTOM_TOSTRING_FORMAT = "%1$s[name=\"%2$s\", %3$s]";
	protected String name = "";
	protected T value = null;

	public Nugget() {
		this(null);
	}

	public Nugget(String name) {
		this(name, null);
	}

	public Nugget(String name, T value) {
		setName(name);
		setValue(value);
	}

	public abstract int getId();

	public abstract void read(DataInputStream in) throws IOException;

	public abstract void write(DataOutputStream out) throws IOException;

	public final void prepWrite(DataOutputStream out) throws IOException {
		out.writeShort(UnsignedConversion.unsignedToSigned(getId()));
		out.writeBoolean(value == null);
		byte[] name_data = getName().getBytes("UTF-8");
		out.writeShort(UnsignedConversion.unsignedToSigned(name_data.length));
		out.write(name_data);
		if (value != null) {
			write(out);
		}
	}

	public final void prepRead(DataInputStream in) throws IOException {
		boolean is_null = in.readBoolean();
		int name_length = UnsignedConversion.signedToUnsigned(in.readShort());
		byte[] name_data = new byte[name_length];
		in.readFully(name_data);
		name = new String(name_data, "UTF-8");
		if (!is_null) {
			read(in);
		}
	}

	@SuppressWarnings("unchecked")
	public static <T extends Nugget<?>> T readNugget(DataInputStream in) throws IOException {
		int nugget_type = UnsignedConversion.signedToUnsigned(in.readShort());
		T nugget = NuggetFactory.newNugget(nugget_type);
		nugget.prepRead(in);
		return nugget;
	}

	public static void writeNugget(Nugget<?> nugget, DataOutputStream out) throws IOException {
		nugget.prepWrite(out);
	}

	public final String getName() {
		return name == null ? "" : name;
	}

	public void setName(String name) {
		this.name = name == null ? "" : name;
	}

	public T getValue() {
		return value;
	}

	public void setValue(T value) {
		this.value = value;
	}

	@Override
	public String toString() {
		return String.format(DEFAULT_TOSTRING_FORMAT, getClass().getSimpleName(), getName(), getValue());
	}

	protected String toString(Object extra_data){
		return String.format(CUSTOM_TOSTRING_FORMAT, getClass().getSimpleName(), getName(), extra_data);
	}

	@Override
	public boolean equals(Object other) {
		if (other == null || !(other instanceof Nugget)) {
			return false;
		}
		Nugget cast = (Nugget) other;
		return cast.getName().equals(getName())
				&& !((cast.value == null && value != null) || (cast.value != null && value == null))
				&& ((value == null) || value.equals(cast.value));
	}
}
