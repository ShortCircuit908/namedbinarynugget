package com.shortcircuit.nbn.nugget;

import com.shortcircuit.nbn.Nugget;
import com.shortcircuit.nbn.UnsignedConversion;

import java.io.*;
import java.net.URI;

/**
 * @author ShortCircuit908
 *         Created on 2/11/2016
 */
public class NuggetFile extends Nugget<File> {
	private static final String TOSTRING_FORMAT = "filename=%1$s, size=%2$s, path=%3$s";
	private String file_name = "";

	public NuggetFile() {
	}

	public NuggetFile(String name) {
		super(name);
	}

	public NuggetFile(String name, File file) {
		this(name);
		setValue(file);
	}

	public NuggetFile(String name, String path) {
		this(name, new File(path));
	}

	public NuggetFile(String name, URI uri) {
		this(name, new File(uri));
	}

	@Override
	public void setValue(File value) {
		this.value = value;
		this.file_name = (value == null ? "" : value.getName());
	}

	public void setValue(String path) {
		setValue(new File(path));
	}

	public void setValue(URI uri) {
		setValue(new File(uri));
	}

	@Override
	public int getId() {
		return 12;
	}

	@Override
	public void read(DataInputStream in) throws IOException {
		long length = UnsignedConversion.signedToUnsigned(in.readInt());
		int file_extension_length = UnsignedConversion.signedToUnsigned(in.readShort());
		byte[] file_name_bytes = new byte[file_extension_length];
		in.readFully(file_name_bytes);
		file_name = new String(file_name_bytes, "UTF-8");
		String extension = file_name.indexOf('.') != -1 ? file_name.substring(file_name.indexOf('.'), file_name.length()) : "";
		value = File.createTempFile("nbn_" + file_name + ";", extension);
		value.deleteOnExit();
		if (length <= 0) {
			return;
		}
		FileOutputStream out = new FileOutputStream(value);
		byte[] buf = new byte[1024];
		int len;
		while ((len = in.read(buf, 0, (int) Math.min(length, buf.length))) > -1 && length > 0) {
			out.write(buf, 0, len);
			length -= len;
		}
		out.flush();
		out.close();
	}

	@Override
	public void write(DataOutputStream out) throws IOException {
		FileInputStream in = new FileInputStream(value);
		out.writeInt(UnsignedConversion.unsignedToSigned(value.length()));
		byte[] file_name_bytes = file_name.getBytes("UTF-8");
		out.writeShort(UnsignedConversion.unsignedToSigned(file_name_bytes.length));
		out.write(file_name_bytes);
		if (value.length() <= 0) {
			return;
		}
		int n;
		byte[] buffer = new byte[1024];
		while ((n = in.read(buffer)) > -1) {
			out.write(buffer, 0, n);
		}
		in.close();
	}

	public String getFileName() {
		return file_name;
	}

	@Override
	public String toString() {
		return toString(
				String.format(TOSTRING_FORMAT,
						value != null ? "\"" + file_name + "\"" : "?",
						value != null && value.exists() ? value.length() + "B" : "?",
						value != null ? "\"" + value.getAbsolutePath() + "\"" : "?"
				)
		);
	}
}
