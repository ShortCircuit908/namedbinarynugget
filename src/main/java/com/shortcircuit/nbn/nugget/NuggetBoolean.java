package com.shortcircuit.nbn.nugget;

import com.shortcircuit.nbn.Nugget;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

/**
 * @author ShortCircuit908
 *         Created on 1/8/2016
 */
public class NuggetBoolean extends Nugget<Boolean> {

	public NuggetBoolean() {
		super();
	}

	public NuggetBoolean(String name) {
		super(name);
	}

	public NuggetBoolean(String name, Boolean value) {
		super(name, value);
	}

	@Override
	public int getId() {
		return 0;
	}

	@Override
	public void read(DataInputStream in) throws IOException {
		value = in.readBoolean();
	}

	@Override
	public void write(DataOutputStream out) throws IOException {
		out.writeBoolean(value);
	}
}
