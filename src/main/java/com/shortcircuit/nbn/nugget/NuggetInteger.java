package com.shortcircuit.nbn.nugget;

import com.shortcircuit.nbn.Nugget;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

/**
 * @author ShortCircuit908
 *         Created on 1/8/2016
 */
public class NuggetInteger extends Nugget<Integer> {

	public NuggetInteger() {
		super();
	}

	public NuggetInteger(String name) {
		super(name);
	}

	public NuggetInteger(String name, Integer value) {
		super(name, value);
	}

	@Override
	public int getId() {
		return 3;
	}

	@Override
	public void read(DataInputStream in) throws IOException {
		value = in.readInt();
	}

	@Override
	public void write(DataOutputStream out) throws IOException {
		out.writeInt(value);
	}
}
