package com.shortcircuit.nbn.nugget;

import com.shortcircuit.nbn.Nugget;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

/**
 * @author ShortCircuit908
 *         Created on 1/8/2016
 */
public class NuggetDouble extends Nugget<Double> {

	public NuggetDouble() {
		super();
	}

	public NuggetDouble(String name) {
		super(name);
	}

	public NuggetDouble(String name, Double value) {
		super(name, value);
	}

	@Override
	public int getId() {
		return 7;
	}

	@Override
	public void read(DataInputStream in) throws IOException {
		value = in.readDouble();
	}

	@Override
	public void write(DataOutputStream out) throws IOException {
		out.writeDouble(value);
	}
}
