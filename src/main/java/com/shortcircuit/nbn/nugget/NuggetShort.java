package com.shortcircuit.nbn.nugget;

import com.shortcircuit.nbn.Nugget;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

/**
 * @author ShortCircuit908
 *         Created on 1/8/2016
 */
public class NuggetShort extends Nugget<Short> {

	public NuggetShort() {
		super();
	}

	public NuggetShort(String name) {
		super(name);
	}

	public NuggetShort(String name, Short value) {
		super(name, value);
	}

	@Override
	public int getId() {
		return 2;
	}

	@Override
	public void read(DataInputStream in) throws IOException {
		value = in.readShort();
	}

	@Override
	public void write(DataOutputStream out) throws IOException {
		out.writeShort(value);
	}
}
