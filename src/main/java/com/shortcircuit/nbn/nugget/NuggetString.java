package com.shortcircuit.nbn.nugget;

import com.shortcircuit.nbn.Nugget;
import com.shortcircuit.nbn.UnsignedConversion;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

/**
 * @author ShortCircuit908
 *         Created on 1/8/2016
 */
public class NuggetString extends Nugget<String> {
	private static final String TOSTRING_FORMAT = "size=%1$s, value=%2$s";

	public NuggetString() {
		super();
	}

	public NuggetString(String name) {
		super(name);
	}

	public NuggetString(String name, String value) {
		super(name, value);
	}

	@Override
	public int getId() {
		return 8;
	}

	@Override
	public void read(DataInputStream in) throws IOException {
		int length = UnsignedConversion.signedToUnsigned(in.readShort());
		byte[] data = new byte[length];
		in.readFully(data);
		value = new String(data, "UTF-8");
	}

	@Override
	public void write(DataOutputStream out) throws IOException {
		byte[] data = value.getBytes("UTF-8");
		out.writeShort(UnsignedConversion.unsignedToSigned(data.length));
		out.write(data);
	}

	@Override
	public String toString() {
		return toString(
				String.format(TOSTRING_FORMAT,
						value != null ? value.length() : "?",
						value != null ? "\"" + value + "\"" : null
				)
		);
	}
}
