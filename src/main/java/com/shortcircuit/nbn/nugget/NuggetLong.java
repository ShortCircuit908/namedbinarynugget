package com.shortcircuit.nbn.nugget;

import com.shortcircuit.nbn.Nugget;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

/**
 * @author ShortCircuit908
 *         Created on 1/8/2016
 */
public class NuggetLong extends Nugget<Long> {

	public NuggetLong() {
		super();
	}

	public NuggetLong(String name) {
		super(name);
	}

	public NuggetLong(String name, Long value) {
		super(name, value);
	}

	@Override
	public int getId() {
		return 6;
	}

	@Override
	public void read(DataInputStream in) throws IOException {
		value = in.readLong();
	}

	@Override
	public void write(DataOutputStream out) throws IOException {
		out.writeLong(value);
	}
}
