package com.shortcircuit.nbn.nugget;

import com.shortcircuit.nbn.UnsignedConversion;
import com.shortcircuit.nbn.Nugget;
import com.shortcircuit.nbn.NuggetFactory;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * @author ShortCircuit908
 *         Created on 1/8/2016
 */
public class NuggetMap<K extends Nugget<?>, V extends Nugget<?>> extends Nugget<Map<K, V>> {
	private static final String TOSTRING_FORMAT = "size=%1$s, key_type=%2$s, value_type=%3$s, value=%4$s";
	private Class<K> key_class;
	private Class<V> value_class;

	public NuggetMap() {

	}

	public NuggetMap(Class<K> key_class, Class<V> value_class) {
		super();
		this.key_class = key_class;
		this.value_class = value_class;
	}

	public NuggetMap(String name, Class<K> key_class, Class<V> value_class) {
		super(name);
		this.key_class = key_class;
		this.value_class = value_class;
	}

	public NuggetMap(String name, Map<K, V> value, Class<K> key_class, Class<V> value_class) {
		super(name, value);
		this.key_class = key_class;
		this.value_class = value_class;
	}

	@Override
	public int getId() {
		return 10;
	}

	@Override
	public void read(DataInputStream in) throws IOException {
		int key_type = UnsignedConversion.signedToUnsigned(in.readShort());
		int value_type = UnsignedConversion.signedToUnsigned(in.readShort());
		int size = UnsignedConversion.signedToUnsigned(in.readShort());
		key_class = NuggetFactory.getNuggetClass(key_type);
		value_class = NuggetFactory.getNuggetClass(value_type);
		value = new HashMap<K, V>(size);
		boolean is_null;
		K map_key;
		V map_value;
		for (int i = 0; i < size; i++) {
			is_null = in.readBoolean();
			if (!is_null) {
				map_key = NuggetFactory.newNugget(key_class);
				map_key.read(in);
			}
			else {
				map_key = null;
			}
			is_null = in.readBoolean();
			if (!is_null) {
				map_value = NuggetFactory.newNugget(value_class);
				map_value.read(in);
			}
			else {
				map_value = null;
			}
			value.put(map_key, map_value);
		}
	}

	public int getKeyType() {
		return NuggetFactory.newNugget(key_class).getId();
	}

	public int getValueType() {
		return NuggetFactory.newNugget(value_class).getId();
	}

	@Override
	@SuppressWarnings("unchecked")
	public void write(DataOutputStream out) throws IOException {
		out.writeShort(UnsignedConversion.unsignedToSigned(getKeyType()));
		out.writeShort(UnsignedConversion.unsignedToSigned(getValueType()));
		out.writeShort(UnsignedConversion.unsignedToSigned(value.size()));
		K map_key;
		V map_value;
		for (Map.Entry<K, V> entry : value.entrySet()) {
			if (entry == null) {
				out.writeBoolean(true);
				out.writeBoolean(true);
				continue;
			}
			map_key = entry.getKey();
			map_value = entry.getValue();
			if (map_key == null) {
				out.writeBoolean(true);
			}
			else {
				out.writeBoolean(false);
				map_key.write(out);
			}
			if (map_value == null) {
				out.writeBoolean(true);
			}
			else {
				out.writeBoolean(false);
				map_value.write(out);
			}
		}
	}

	@Override
	public String toString() {
		return toString(
				String.format(TOSTRING_FORMAT,
						value != null ? value.size() : "?",
						key_class != null ? getKeyType() : "?",
						value_class != null ? getValueType() : "?",
						value
				)
		);
	}

	public void setKeyValueClasses(Class<K> key_class, Class<V> value_class) {
		this.key_class = key_class;
		this.value_class = value_class;
	}
}
