package com.shortcircuit.nbn.nugget;

import com.shortcircuit.nbn.Nugget;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

/**
 * @author ShortCircuit908
 *         Created on 1/8/2016
 */
public class NuggetFloat extends Nugget<Float> {

	public NuggetFloat() {
		super();
	}

	public NuggetFloat(String name) {
		super(name);
	}

	public NuggetFloat(String name, Float value) {
		super(name, value);
	}

	@Override
	public int getId() {
		return 4;
	}

	@Override
	public void read(DataInputStream in) throws IOException {
		value = in.readFloat();
	}

	@Override
	public void write(DataOutputStream out) throws IOException {
		out.writeFloat(value);
	}
}
