package com.shortcircuit.nbn.nugget;

import com.shortcircuit.nbn.Nugget;
import com.shortcircuit.nbn.NuggetFactory;
import com.shortcircuit.nbn.UnsignedConversion;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.lang.reflect.Array;
import java.util.Arrays;

/**
 * @author ShortCircuit908
 *         Created on 1/8/2016
 */
public class NuggetArray<T, E extends Nugget<T>> extends Nugget<E[]> {
	private static final String TOSTRING_FORMAT = "size=%1$s, type=%2$s, value=%3$s";

	public NuggetArray() {
		super();
	}

	public NuggetArray(String name) {
		super(name);
	}

	public NuggetArray(String name, E[] value) {
		super(name, value);
	}

	@Override
	public int getId() {
		return 9;
	}

	@Override
	@SuppressWarnings("unchecked")
	public void read(DataInputStream in) throws IOException {
		int element_type = UnsignedConversion.signedToUnsigned(in.readShort());
		int size = UnsignedConversion.signedToUnsigned(in.readShort());
		Class<E> nugget_class = NuggetFactory.getNuggetClass(element_type);
		value = (E[]) Array.newInstance(nugget_class, size);
		boolean is_null;
		E element;
		for (int i = 0; i < size; i++) {
			is_null = in.readBoolean();
			if (!is_null) {
				element = NuggetFactory.newNugget(nugget_class);
				element.read(in);
				Array.set(value, i, element);
			}
		}
	}

	@Override
	public void write(DataOutputStream out) throws IOException {
		out.writeShort(UnsignedConversion.unsignedToSigned(getElementType()));
		out.writeShort(UnsignedConversion.unsignedToSigned(value.length));
		for (E element : value) {
			if (element == null) {
				out.writeBoolean(true);
				continue;
			}
			out.writeBoolean(false);
			element.write(out);
		}
	}

	@SuppressWarnings("unchecked")
	public int getElementType() {
		Class<E> nugget_class = (Class<E>) value.getClass().getComponentType();
		return NuggetFactory.newNugget(nugget_class).getId();
	}

	@Override
	public String toString() {
		return toString(
				String.format(TOSTRING_FORMAT,
						value != null ? value.length : "?",
						value != null ? getElementType() : "?",
						Arrays.deepToString(value)));
	}

	@Override
	public boolean equals(Object other) {
		if (other == null || !(other instanceof NuggetArray)) {
			return false;
		}
		NuggetArray<T, E> cast;
		try {
			cast = (NuggetArray<T, E>) other;
		}
		catch (ClassCastException e) {
			return false;
		}
		return cast.getName().equals(getName()) && Arrays.deepEquals(value, cast.value);
	}
}
