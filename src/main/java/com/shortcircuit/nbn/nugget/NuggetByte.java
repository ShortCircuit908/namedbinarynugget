package com.shortcircuit.nbn.nugget;

import com.shortcircuit.nbn.Nugget;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

/**
 * @author ShortCircuit908
 *         Created on 1/8/2016
 */
public class NuggetByte extends Nugget<Byte> {

	public NuggetByte() {
		super();
	}

	public NuggetByte(String name) {
		super(name);
	}

	public NuggetByte(String name, Byte value) {
		super(name, value);
	}

	@Override
	public int getId() {
		return 1;
	}

	@Override
	public void read(DataInputStream in) throws IOException {
		value = in.readByte();
	}

	@Override
	public void write(DataOutputStream out) throws IOException {
		out.writeByte(value);
	}
}
