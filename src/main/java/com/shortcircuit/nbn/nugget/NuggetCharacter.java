package com.shortcircuit.nbn.nugget;

import com.shortcircuit.nbn.Nugget;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

/**
 * @author ShortCircuit908
 *         Created on 1/8/2016
 */
public class NuggetCharacter extends Nugget<Character> {

	public NuggetCharacter() {
		super();
	}

	public NuggetCharacter(String name) {
		super(name);
	}

	public NuggetCharacter(String name, Character value) {
		super(name, value);
	}

	@Override
	public int getId() {
		return 5;
	}

	@Override
	public void read(DataInputStream in) throws IOException {
		value = in.readChar();
	}

	@Override
	public void write(DataOutputStream out) throws IOException {
		out.writeChar(value);
	}

	@Override
	public String toString(){
		return toString("value=" + (value != null ? "'" + value + "'" : null));
	}
}
