package com.shortcircuit.nbn.nugget;

import com.shortcircuit.nbn.UnsignedConversion;
import com.shortcircuit.nbn.Nugget;
import com.shortcircuit.nbn.NuggetFactory;
import com.sun.istack.internal.NotNull;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.Arrays;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

/**
 * @author ShortCircuit908
 *         Created on 1/8/2016
 */
public class NuggetCompound extends Nugget<List<Nugget<?>>> {
	private static final String TOSTRING_FORMAT = "size=%1$s, value=%2$s";

	public NuggetCompound() {
		this.name = "";
	}

	public NuggetCompound(String name) {
		super(name);
	}

	public NuggetCompound(List<Nugget<?>> value) {
		this();
		this.value = value;
	}

	public NuggetCompound(String name, List<Nugget<?>> value) {
		super(name, value);
	}

	@Override
	public int getId() {
		return 11;
	}

	@Override
	public void read(DataInputStream in) throws IOException {
		int size = UnsignedConversion.signedToUnsigned(in.readShort());
		int nugget_type;
		value = new LinkedList<Nugget<?>>();
		Nugget<?> nugget;
		for (int i = 0; i < size; i++) {
			nugget_type = UnsignedConversion.signedToUnsigned(in.readShort());
			nugget = NuggetFactory.newNugget(NuggetFactory.getNuggetClass(nugget_type));
			nugget.prepRead(in);
			value.add(nugget);
		}
	}

	@Override
	public void write(DataOutputStream out) throws IOException {
		out.writeShort(UnsignedConversion.unsignedToSigned(value.size()));
		for (Nugget<?> nugget : value) {
			nugget.prepWrite(out);
		}
	}

	public Nugget<?> getNugget(String name) {
		if (value == null) {
			return null;
		}
		for (Nugget<?> nugget : value) {
			if (nugget.getName().equals(name)) {
				return nugget;
			}
		}
		return null;
	}

	public Nugget<?> getNugget(int index) throws IndexOutOfBoundsException {
		if (value == null) {
			return null;
		}
		return value.get(index);
	}

	public void addNugget(@NotNull Nugget<?> nugget) {
		if (value == null) {
			value = new LinkedList<Nugget<?>>();
		}
		value.add(nugget);
	}

	public boolean removeNugget(Nugget<?> nugget) {
		return value != null && value.remove(nugget);
	}

	public Nugget<?> removeNugget(int index) throws IndexOutOfBoundsException {
		if (value == null) {
			return null;
		}
		return value.remove(index);
	}

	public Nugget<?> removeNugget(String name) {
		if (value == null) {
			return null;
		}
		Iterator<Nugget<?>> iterator = value.iterator();
		while (iterator.hasNext()) {
			Nugget<?> nugget = iterator.next();
			if (nugget.getName().equals(name)) {
				iterator.remove();
				return nugget;
			}
		}
		return null;
	}

	public int getSize() {
		return value == null ? 0 : value.size();
	}

	public boolean isEmpty() {
		return value == null || value.isEmpty();
	}

	public LinkedList<String> getNuggetNames() {
		LinkedList<String> names = new LinkedList<String>();
		if (value == null) {
			return names;
		}
		for (Nugget<?> nugget : value) {
			if (nugget == null) {
				continue;
			}
			names.add(nugget.getName());
		}
		return names;
	}

	@Override
	public String toString() {
		return toString(
				String.format(TOSTRING_FORMAT,
						value != null ? value.size() : "?",
						value != null ? Arrays.deepToString(value.toArray()) : null
				)
		);
	}

	@Override
	public boolean equals(Object other) {
		if (other == null || !(other instanceof NuggetCompound)) {
			return false;
		}
		NuggetCompound cast = (NuggetCompound) other;
		if (!cast.getName().equals(getName()) || cast.getSize() != getSize()) {
			return false;
		}
		for (int i = 0; i < getSize(); i++) {
			Nugget<?> nugget = getNugget(i);
			Nugget<?> other_nugget = cast.getNugget(i);
			if (nugget == null) {
				if (other_nugget != null) {
					return false;
				}
				continue;
			}
			if (!nugget.equals(other_nugget)) {
				return false;
			}
		}
		return true;
	}
}
