package com.shortcircuit.nbn;

import com.shortcircuit.nbn.nugget.*;

import java.io.File;
import java.lang.reflect.Array;
import java.lang.reflect.Constructor;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * @author ShortCircuit908
 *         Created on 1/8/2016
 */
public class NuggetFactory {
	private static final LinkedHashMap<Integer, Class<? extends Nugget<?>>> by_id = new LinkedHashMap<Integer, Class<? extends Nugget<?>>>();
	private static final LinkedHashMap<Class<?>, Class<? extends Nugget<?>>> by_data_type = new LinkedHashMap<Class<?>, Class<? extends Nugget<?>>>();

	static {
		registerNugget(NuggetBoolean.class, Boolean.class);
		registerNugget(NuggetBoolean.class, boolean.class);
		registerNugget(NuggetByte.class, Byte.class);
		registerNugget(NuggetByte.class, byte.class);
		registerNugget(NuggetCharacter.class, Character.class);
		registerNugget(NuggetCharacter.class, char.class);
		registerNugget(NuggetDouble.class, Double.class);
		registerNugget(NuggetDouble.class, double.class);
		registerNugget(NuggetFloat.class, Float.class);
		registerNugget(NuggetFloat.class, float.class);
		registerNugget(NuggetInteger.class, Integer.class);
		registerNugget(NuggetInteger.class, int.class);
		registerNugget(NuggetLong.class, Long.class);
		registerNugget(NuggetLong.class, long.class);
		registerNugget(NuggetShort.class, Short.class);
		registerNugget(NuggetShort.class, short.class);
		registerNugget(NuggetString.class, String.class);
		registerNugget(NuggetArray.class, Nugget[].class);
		registerNugget(NuggetMap.class, Map.class);
		registerNugget(NuggetCompound.class, Nugget.class);
		registerNugget(NuggetCompound.class, Object.class);
		registerNugget(NuggetFile.class, File.class);
	}

	public static <T, E extends Nugget<?>> void registerNugget(Class<E> nugget_class, Class<T> data_type) {
		try {
			Constructor<E> ctor = nugget_class.getConstructor();
			ctor.setAccessible(true);
			E nugget = ctor.newInstance();
			by_id.put(nugget.getId(), nugget_class);
			by_data_type.put(data_type, nugget_class);
		}
		catch (Exception e) {
			throw new IllegalArgumentException("Class does not contain a no-args constructor!", e);
		}
	}

	@SuppressWarnings("unchecked")
	public static <E extends Nugget> Class<E> getNuggetClass(int nugget_type) {
		if (by_id.containsKey(nugget_type)) {
			return (Class<E>) by_id.get(nugget_type);
		}
		throw new IllegalArgumentException("No Nugget registered with ID " + nugget_type);
	}

	@SuppressWarnings("unchecked")
	private static <E extends Nugget> Class<E> getNuggetClass(Class<?> data_type) {
		if (by_data_type.get(data_type) != null) {
			return (Class<E>) by_data_type.get(data_type);
		}
		if (data_type.isArray()) {
			registerNugget(NuggetArray.class, data_type);
			return (Class<E>) NuggetArray.class;
		}
		throw new IllegalArgumentException("No Nugget registered for type " + data_type.getName());
	}

	@SuppressWarnings("unchecked")
	public static <E extends Nugget<?>> E newNugget(int nugget_type) {
		return (E) newNugget(getNuggetClass(nugget_type));
	}

	public static <E extends Nugget<?>> E newNugget(Class<E> nugget_type) {
		try {
			Constructor<E> ctor = nugget_type.getConstructor();
			ctor.setAccessible(true);
			return ctor.newInstance();
		}
		catch (Exception e) {
			throw new IllegalArgumentException("Class does not contain a no-args constructor!", e);
		}
	}

	public static <T, E extends Nugget<T>> NuggetArray<T, E> wrapNuggetArray(Object array) {
		return wrapNuggetArray(array, null);
	}

	@SuppressWarnings("unchecked")
	public static <T, E extends Nugget<T>> NuggetArray<T, E> wrapNuggetArray(Object array, String name) {
		NuggetArray<T, E> nugget_array = new NuggetArray<T, E>(name);
		if (array == null) {
			return nugget_array;
		}
		Class<E> nugget_class = getNuggetClass(array.getClass().getComponentType());
		E[] raw_value = (E[]) Array.newInstance(nugget_class, Array.getLength(array));
		for (int i = 0; i < Array.getLength(array); i++) {
			E value_nugget = NuggetFactory.newNugget(nugget_class);
			value_nugget.setValue((T) Array.get(array, i));
			raw_value[i] = value_nugget;
		}
		nugget_array.setValue(raw_value);
		return nugget_array;
	}

	@SuppressWarnings("unchecked")
	public static <T, E extends Nugget<T>> T[] unwrapNuggetArray(NuggetArray<T, E> nugget_array) {
		if (nugget_array.value == null) {
			return null;
		}
		T[] array = null;
		for (int i = 0; i < nugget_array.value.length; i++) {
			E nugget = nugget_array.value[i];
			if (array == null && nugget.value != null) {
				array = (T[]) Array.newInstance(nugget.value.getClass(), nugget_array.value.length);
			}
			if (array != null) {
				Array.set(array, i, nugget.value);
			}
		}
		return array;
	}
}
