package com.shortcircuit.nbn;

/**
 * @author ShortCircuit908
 *         Created on 1/8/2016
 */
public class UnsignedConversion {
	public static short signedToUnsigned(byte v) {
		return (short) (((short) v) & 0xff);
	}

	public static byte unsignedToSigned(short v) {
		return (byte) (v | ~0xff);
	}

	public static int signedToUnsigned(short v) {
		return ((int) v) & 0xffff;
	}

	public static short unsignedToSigned(int v) {
		return (short) (v | ~0xffff);
	}

	public static long signedToUnsigned(int v) {
		return ((long) v) & 0xffffffffL;
	}

	public static int unsignedToSigned(long v) {
		return (int) (v | ~0xffffffffL);
	}

	public static double signedBitsToUnsigned(int v) {
		return Double.longBitsToDouble(signedToUnsigned(v));
	}

	public static int unsignedToSignedBits(double v) {
		return unsignedToSigned(Double.doubleToRawLongBits(v));
	}

	public static float signedBitsToUnsigned(short v) {
		return Float.intBitsToFloat(signedToUnsigned(v));
	}

	public static short unsignedToSignedBits(float v) {
		return unsignedToSigned(Float.floatToRawIntBits(v));
	}
}
