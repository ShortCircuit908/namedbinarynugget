package com.shortcircuit.nbn;

import java.lang.reflect.Array;

/**
 * @author ShortCircuit908
 *         Created on 1/11/2016
 */
public class ArrayCasting {
	public static Boolean[] primitiveToWrapper(boolean[] array) {
		if (array == null) {
			return null;
		}
		Boolean[] cast = new Boolean[array.length];
		for (int i = 0; i < array.length; i++) {
			cast[i] = array[i];
		}
		return cast;
	}

	public static Byte[] primitiveToWrapper(byte[] array) {
		if (array == null) {
			return null;
		}
		Byte[] cast = new Byte[array.length];
		for (int i = 0; i < array.length; i++) {
			cast[i] = array[i];
		}
		return cast;
	}

	public static Short[] primitiveToWrapper(short[] array) {
		if (array == null) {
			return null;
		}
		Short[] cast = new Short[array.length];
		for (int i = 0; i < array.length; i++) {
			cast[i] = array[i];
		}
		return cast;
	}

	public static Character[] primitiveToWrapper(char[] array) {
		if (array == null) {
			return null;
		}
		Character[] cast = new Character[array.length];
		for (int i = 0; i < array.length; i++) {
			cast[i] = array[i];
		}
		return cast;
	}

	public static Integer[] primitiveToWrapper(int[] array) {
		if (array == null) {
			return null;
		}
		Integer[] cast = new Integer[array.length];
		for (int i = 0; i < array.length; i++) {
			cast[i] = array[i];
		}
		return cast;
	}

	public static Float[] primitiveToWrapper(float[] array) {
		if (array == null) {
			return null;
		}
		Float[] cast = new Float[array.length];
		for (int i = 0; i < array.length; i++) {
			cast[i] = array[i];
		}
		return cast;
	}

	public static Long[] primitiveToWrapper(long[] array) {
		if (array == null) {
			return null;
		}
		Long[] cast = new Long[array.length];
		for (int i = 0; i < array.length; i++) {
			cast[i] = array[i];
		}
		return cast;
	}

	public static Double[] primitiveToWrapper(double[] array) {
		if (array == null) {
			return null;
		}
		Double[] cast = new Double[array.length];
		for (int i = 0; i < array.length; i++) {
			cast[i] = array[i];
		}
		return cast;
	}

	public static boolean[] wrapperToPrimitive(Boolean[] array) {
		if (array == null) {
			return null;
		}
		boolean[] cast = new boolean[array.length];
		for (int i = 0; i < array.length; i++) {
			cast[i] = array[i];
		}
		return cast;
	}

	public static byte[] wrapperToPrimitive(Byte[] array) {
		if (array == null) {
			return null;
		}
		byte[] cast = new byte[array.length];
		for (int i = 0; i < array.length; i++) {
			cast[i] = array[i];
		}
		return cast;
	}

	public static short[] wrapperToPrimitive(Short[] array) {
		if (array == null) {
			return null;
		}
		short[] cast = new short[array.length];
		for (int i = 0; i < array.length; i++) {
			cast[i] = array[i];
		}
		return cast;
	}

	public static char[] wrapperToPrimitive(Character[] array) {
		if (array == null) {
			return null;
		}
		char[] cast = new char[array.length];
		for (int i = 0; i < array.length; i++) {
			cast[i] = array[i];
		}
		return cast;
	}

	public static int[] wrapperToPrimitive(Integer[] array) {
		if (array == null) {
			return null;
		}
		int[] cast = new int[array.length];
		for (int i = 0; i < array.length; i++) {
			cast[i] = array[i];
		}
		return cast;
	}

	public static float[] wrapperToPrimitive(Float[] array) {
		if (array == null) {
			return null;
		}
		float[] cast = new float[array.length];
		for (int i = 0; i < array.length; i++) {
			cast[i] = array[i];
		}
		return cast;
	}

	public static long[] wrapperToPrimitive(Long[] array) {
		if (array == null) {
			return null;
		}
		long[] cast = new long[array.length];
		for (int i = 0; i < array.length; i++) {
			cast[i] = array[i];
		}
		return cast;
	}

	public static double[] wrapperToPrimitive(Double[] array) {
		if (array == null) {
			return null;
		}
		double[] cast = new double[array.length];
		for (int i = 0; i < array.length; i++) {
			cast[i] = array[i];
		}
		return cast;
	}

	@SuppressWarnings("unchecked")
	public static <T, E> E[] castArray(T[] array, Class<E> element_class) {
		if (array == null) {
			return null;
		}
		E[] cast = (E[]) Array.newInstance(element_class, array.length);
		for (int i = 0; i < array.length; i++) {
			cast[i] = element_class.cast(array[i]);
		}
		return cast;
	}
}
